# ÚVZ

Projekt na podporu INSPIRE aktivít Úradu verejného zdravotníctva

Pracovné stretnutia:

- 03.07.2020: Telekonferencia END & INSPIRE [Zápis z pracovného stretnutia](http://inspire.gov.sk/Upload/interoperability/meetings/20200703_BB_MZPSR_UVZ_SAZP_IPD_END/20200703_BB_MZPSR_UVZ_SAZP_IPD_END_Zapis_final.pdf)
- 31.07.2020: [Pracovné stretnutie k problematike END, IS Pitná voda a IS vody na kúpanie](https://drive.geocloud.sk/s/wTeJzK4EFQ4bkk3)
- 12.11.2020: Pracovné stretnutie k END, INSPIRE a pre Národný projekt: Integrovaný systém Úradov verejného zdravotníctva SR
